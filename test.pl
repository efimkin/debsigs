# A simple module loading test

use Test::More tests => 4;

foreach (qw/debsigsmain arf forktools gpg/) {
	use_ok("Debian::debsigs::$_");
}
